#include <iostream>
#include <fstream>
using namespace std;

void save(const char* fileName);

int main()
{
	cout << "이 파일을 하드디스크에 저장하도록 하자" << endl;

	save("파일입출력.cpp");
}

void save(const char* fileName)
{
	// 내용을 읽을 파일을 연다 -> "소스.cpp"
	ifstream in(fileName);
	// 읽은 내용을 저장할 파일을 연다 -> 저장할 파일이름을 "강의저장.txt"라고 하자
	ofstream out("강의저장.txt",ios::app);
	// 소스.cpp의 모든 내용을 읽어 강의저장.txt 파일에 기록한다.

	int c;
	while ((c = in.get()) != EOF)
	{
		out << (char)c;
	}
	out << endl;

	cout << fileName <<"를 강의저장.txt에 저장하였습니다." << endl;
}