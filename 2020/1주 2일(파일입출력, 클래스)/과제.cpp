// [1주 2일 - 과제]
// [문제 1] 정수1000개.txt가 차지하는 하드디스크 파일의 크기를 적어라. A.11.489 Byte
// [문제 2] 정수 1000개를 최소한의 하드디스크 크기를 사용하도록 하려면 무엇을 어떻게 해야
// 하는 지 설명하라.
// 바이너리로 저장 및 '\0'를 제외하면 4000byte 최소용량이 나온다.

#include <iostream>
#include <random>
#include <fstream>
using namespace std;

void inData(const char* fileName)
{
	cout << "== 바이너리 랜덤정수 파일입력 시작 ==" << endl;

	ifstream in(fileName, ios::binary);

	if (!in) return;

	in.seekg(0, in.end);
	int length = (int)in.tellg() / sizeof(int);
	in.seekg(0, in.beg);

	int a;
	for (int i = 0; i < length; ++i)
	{
		in.read(reinterpret_cast<char*>(&a), sizeof(int));
		//cout << a << endl;

	}

	in.close();
}

void outData(const char* fileName)
{
	cout << "== 바이너리 랜덤정수 파일출력 시작 ==" << endl;

	ofstream out(fileName, ios::binary);
	default_random_engine dre;
	uniform_int_distribution<int> uid;

	int n;
	for (int i = 0; i < 1000; ++i)
	{
		n = uid(dre);
		out.write(reinterpret_cast<const char*>(&n), sizeof(int));
		//cout << n << endl;
	}
}

int main()
{
	outData("정수1000개.txt");
	inData("정수1000개.txt");
}