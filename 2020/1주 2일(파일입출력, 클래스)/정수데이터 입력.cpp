#include <iostream>
#include <fstream>
using namespace std;

#define INFINITE_MINUS -99999

int main()
{
	ifstream in("정수1000개.txt");

	int n;
	int i{ 0 };

	int max = INFINITE_MINUS;

	while (in >> n) 
	{
		if (max < n) max = n;

		cout << ++i << " - " << n << endl;
	}

	// [실습] 정수1000개.txt에 저장된 값 중 가장 큰 값을 찾아 화면에 출력하세요.
	cout << max << endl;

	// [질문] 정수 1000개를 저장하려면 얼마만큼 메모리가 필요합니까?
	// 4byte * 1000 = 4000byte
	// cout << sizeof(int)*1000 << endl;
	// int a[1000];
	// cout << sizeof(a) << endl;

	// [실습] 정수1000개.txt 파일의 크기를 확인하세요.
	// 11.489 Byte




}