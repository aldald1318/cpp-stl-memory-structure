#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
using namespace std;


string file{ "STL Wiki.txt" };

int main()
{
	// file 읽기
	ifstream in(file);
	if (!in) {
		cout << file << " 파일 열기 실패" << endl;
		return 0;
	}

	vector<string> sv;

	std::string temp;
	while (in >> temp)
		sv.emplace_back(temp);

	ofstream out("STL Wiki에서 5글자로 된 단어.txt");
	for (auto& p : sv)
	{
		if (p.length() == 5) {
			out << p;
			out << ' ';
		}
	}
}