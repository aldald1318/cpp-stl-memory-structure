#pragma once

namespace wt
{
	template <class T>
	class unique_ptr
	{
	private:
		T* pointer;

	public:
		// 기본생성자
		unique_ptr() {}
		unique_ptr(T* ptr) : pointer(ptr) {}
		~unique_ptr()
		{
			if (pointer != nullptr) delete pointer;
			pointer = nullptr;
		}
		// 복사생성자 삭제 (이동만 가능하도록)
		explicit unique_ptr(const unique_ptr& rhs) = delete;
		unique_ptr& operator= (const unique_ptr& rhs) = delete;

		// 이동생성자
		unique_ptr(unique_ptr&& rhs)
		{
			if (this != std::addressof(rhs))
			{
				T* rhsPtr = rhs.release();
				reset(rhsPtr);
			}
		}
		unique_ptr& operator= (unique_ptr&& rhs)
		{
			// if(this != r value) reset -> swap
			if (this != std::addressof(rhs))
			{
				T* rhsPtr = rhs.release();
				reset(rhsPtr);
			}
			return *this;
		}

		/*
		@ 객체를 초기화 해준다.
		*/
		void reset(T* newPointer = nullptr)
		{
			if(pointer) delete pointer;
			pointer = newPointer;
		}

		/* 주의! 객체를 반환시 꼭 메모리 해제를 해주어야한다.
		@ 현재 객체 포인터를 반환하고,
		@ 디폴트 생성자를 저장하여 객체를 초기화한다.
		@ return 초기화 전 객체 포인터
		*/
		T* release()
		{
			// val: cur value / newVal: default
			T* oldValue = pointer;
			pointer = nullptr;
			return oldValue;
		}

		// pointer 접근 메서드 및 연산자
		T* get() const { return pointer; };
		T* operator-> () const { return pointer; }
	};

	template <class T, class... Args>
	unique_ptr<T> make_unique(Args... args)
	{
		return unique_ptr<T>(new T(args...));
	}
}


