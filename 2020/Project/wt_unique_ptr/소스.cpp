#include <iostream>
#include <Windows.h>
#include "wt_unique_ptr.h"
using namespace std;

class TEST
{
public:
	TEST() = default;
	TEST(int ta) 
	{ 
		a = ta; 
	}
	~TEST()
	{
	};

public:
	int a;
};

int main()
{
	//{
	//	wt::unique_ptr<TEST> wt;
	//	wt = wt::make_unique<TEST>(3);
	//	TEST* ptr = wt.get();
	//	cout << ptr << endl;

	//	wt = wt::make_unique<TEST>(4);
	//	cout << ptr << endl;
	//}

	std::unique_ptr<TEST> tt = std::make_unique<TEST>(4);
	tt.reset();

	if (tt)
		cout << tt->a << endl;
	else
		std::cout << "nullptr�Դϴ�." << std::endl;
	///////////////////////////////////

	// method
	// get
	// release
	// reset
	// ~unique_ptr

	// operator ->
	// operator =

	system("pause");
}