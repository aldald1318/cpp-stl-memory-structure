#include  <iostream>
#include <string>
#include <fstream>
#include <random>
#include <algorithm>
#include <vector>
#include <functional>
using namespace std;

class Name {
	string first{};
	string last{};

public:
	Name() = default;
	Name(string first, string last) : first {first}, last {last} {}

	string& getFirst() {
		return first;
	}

	string getFirst() const {
		return first;
	}

	string getLast() const {
		return last;
	}

	void setLast(const string& name) {
		last = name;
	}

	void show() const {
		cout << first << " " << last << endl;
	}

	const bool operator<(const Name& rhs) {
		return last < rhs.getLast();
	}
};

string file1{ "고객1.txt" }, file2{ "고객2.txt" };

void makeData();
void makeUpper(vector<Name>& customer);

int main()
{
	{
		// 문제에 필요한 데이터를 생성한다.
		makeData();
	}

	// 문제1 고객1.txt 만명의 정보를 읽어 컨테이너에 저장
	vector<Name> custormers1;
	vector<Name> custormers2;

	// 고객1 읽어들이기
	ifstream in;
	int fileSize = 0;
	in.open(file1);
	if (in.is_open())
	{
		in.seekg(0, std::ios::end);
		fileSize = in.tellg();
		in.seekg(0, std::ios::beg);
		custormers1.reserve(fileSize);

		while (!in.eof())
		{
			string first, last;
			in >> first >> last;
			custormers1.emplace_back(first, last);
		}
	}
	in.close();

	// 고객2 읽어들이기
	in.open(file2);
	if (in.is_open())
	{
		in.seekg(0, std::ios::end);
		fileSize = in.tellg();
		in.seekg(0, std::ios::beg);
		custormers2.reserve(fileSize);

		while (!in.eof())
		{
			string first, last;
			in >> first >> last;
			custormers2.emplace_back(first, last);
		}
	}
	in.close();

	// [문제2] 성이 "kim"인 객체가 있는지 찾아라
	// 찾으면 출력
	// 못찾으면 찾지못함이라고 화면출력

	std::find_if(custormers1.begin(), custormers1.end(), [](Name& name) {
		if(name.getLast() == "kim"){
			name.show();
			return false;
		}
		});

	// [문제3] 모든 객체를 이름(first name) 오름차순으로 정렬
	std::sort(custormers1.begin(), custormers1.end(), [](Name& name1, Name& name2) {
		return name1.getFirst() < name2.getFirst();
		});

	// 문제4 makeUppder()
	// 첫 글자를 대문자로 바꾼다.
	makeUpper(custormers1);

	// 문제5
	// sort last name 기준 오름차순 정렬
	std::sort(custormers1.begin(), custormers1.end());

	// 문제6
	// 고객1 과 고객2를 한개의 컨테이너로 합하라
	// 합한 컨테이너의 원소개수 출력
	// 동일한 성(last name)의 Name 객체가 존재해서는 안된다.
	// remove erase 사용해야함
	custormers1.insert(custormers1.end(), custormers2.begin(), custormers2.end());
	custormers1.erase(unique(custormers1.begin(), custormers1.end(), [](Name& name1, Name& name2) {
		return name1.getLast() == name2.getLast();
		}), custormers1.end());

	int i = 0;
	for (auto& p : custormers1)
	{
		//p.show();
		++i;
	}
	cout << "total count: " << i << endl;
}

void makeUpper(vector<Name>& customer)
{
	for (auto& p : customer)
	{
		string name = p.getLast();
		name[0] = toupper(name[0]);
		p.setLast(name);
	}
}

void makeData()
{
	ofstream out1{ file1 };
	ofstream out2{ file2 };

	default_random_engine dre;
	uniform_int_distribution<> uiNameLen(3, 12);
	uniform_int_distribution<> uiName('a', 'z');
	
	for (int i = 0; i < 1'0000; ++i) {
		string first;
		for (int i = 0; i < uiNameLen(dre); ++i)
			first += uiName(dre);

		string second;
		for (int i = 0; i < 3; ++i)
			second += uiName(dre);

		Name name(first, second);
		out1 << name.getFirst() << "\t" << name.getLast() << endl;
	}

	for (int i = 0; i < 2'0000; ++i) {
		string first;
		for (int i = 0; i < uiNameLen(dre); ++i)
			first += uiName(dre);

		string second;
		for (int i = 0; i < 3; ++i)
			second += uiName(dre);

		Name name(first, second);
		out2 << name.getFirst() << "\t" << name.getLast() << endl;
	}
}