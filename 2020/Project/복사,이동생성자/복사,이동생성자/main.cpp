#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Model {
public:
	// 생성, 소멸, 복사, 복사할당, 이동생성, 이동할당
	Model(){ cout << this << " 생성자 호출" << endl; }
	Model(size_t dataSize)
	{
		cout << this << " 파라미터 생성자 호출" << endl;
		size = dataSize;
		data = new char(size);
	}

	// 복사
	Model(const Model& other)
	{
		cout << this << " 복사연산자 호출" << endl;
		size = other.size;
		data = other.data;
	}
	Model& operator=(const Model& other)
	{
		cout << this << " 복사할당연산자" << endl;
		size = other.size;
		data = other.data;
		return *this;
	}

	// 이동
	Model(Model&& other) noexcept : 
		data(nullptr), size(other.size)
	{
		// 메모리 이동
		data = other.data;

		// 안해주면 댕글링 포인터가 될수있음
		other.data = nullptr;
		other.size = 0;

		cout << this << " 이동생성자 호출" << endl;
	}

	Model& operator=(Model&& other) noexcept
	{
		// 메모리 이동
		size = other.size;
		data = other.data;

		// 안해주면 댕글링 포인터가 될수있음
		other.data = nullptr;
		other.size = 0;

		cout << this << " 이동대입연산자" << endl;
		return *this;
	}

	~Model() { cout << this << " 소멸자 호출" << endl; }

public:
	char* data;
	size_t size;
};

int main()
{
	Model a{ 1000 };
	// move역할이 객체의 타입을 &&로 만들어주는것이다.
	Model b = move(a);
	
	// 이동할당연산자
	Model c;
	c = static_cast<Model&&>(b);

}
