#pragma once

class Player
{
private:
	string m_ID;
	unsigned int m_Breakout;			// ��Ż�� ����
	unsigned int m_ChampionsLeague;	// è�Ǿ𽺸��� ����

public:
	string GetID() const;

	void SetBreakoutScore(unsigned int score);
	unsigned int GetBreakoutScore() const;

	void SetChampionsLeagueScore(unsigned int score);
	unsigned int GetChampionsLeagueScore() const;

public:
	void UpdateBreakout(unsigned int breakout);
	void UpdateChampionsLeague(unsigned int championsLeague);

public:
	explicit Player(std::string id);
	virtual ~Player();
};

