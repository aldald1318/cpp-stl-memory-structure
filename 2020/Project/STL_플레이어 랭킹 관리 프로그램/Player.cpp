#include "stdafx.h"
#include "Player.h"

Player::Player(std::string id) : m_ID(id),
	m_Breakout(0), m_ChampionsLeague(0)
{
}

Player::~Player()
{
}

string Player::GetID() const
{
	return m_ID;
}

void Player::SetBreakoutScore(unsigned int score)
{
	m_Breakout = score;
}

unsigned int Player::GetBreakoutScore() const
{
	return m_Breakout;
}

void Player::SetChampionsLeagueScore(unsigned int score)
{
	m_ChampionsLeague = score;
}

unsigned int Player::GetChampionsLeagueScore() const
{
	return m_ChampionsLeague;
}

void Player::UpdateBreakout(unsigned int breakout)
{
	// 갱신된 점수가 기존점수보다 높을경우만 업데이트를한다.
	if(breakout > m_Breakout)
		m_Breakout = breakout;
}

void Player::UpdateChampionsLeague(unsigned int championsLeague)
{
	// 갱신된 점수가 기존점수보다 높을경우만 업데이트를한다.
	if (championsLeague > m_ChampionsLeague)
		m_ChampionsLeague = championsLeague;
}