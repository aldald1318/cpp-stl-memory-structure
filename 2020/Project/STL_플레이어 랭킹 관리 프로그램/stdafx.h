#pragma once


#include <iostream>
#include <iterator>
#include <algorithm>
#include <functional>
#include <vector>
#include <map>
#include <random>
#include <chrono>
#include <cassert>
#include <set>
#include <deque>
#include <list>

using namespace std;
using namespace chrono;

template<class T>
constexpr const T& clamp(const T& v, const T& lo, const T& hi)
{
	assert(!(hi < lo));
	return (v < lo) ? lo : (hi < v) ? hi : v;
}
