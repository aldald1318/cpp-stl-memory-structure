#include <iostream>
#include <Windows.h>
#include <vector>

using namespace std;

class TEST {
private:
	int data;

public:
	// 복사 생성자
	TEST(const TEST& other)
	{
		cout << this << " - 복사연산자 호출" << endl;
		data = other.data;
	}
	TEST& operator=(const TEST& other)
	{
		cout << this << " - 복사할당연산자" << endl;
		data = other.data;
		return *this;
	}
	// 이동생성자
	TEST(TEST&& other)
	{
		// 기존 데이터 메모리해제
		if (data) data = NULL;

		// 데이터 복사
		data = other.data;

		// 이동 데이터 메모리해제
		other.data = NULL;

		cout << this << " - 이동생성자 호출" << endl;
	}

	TEST& operator=(TEST&& other)
	{
		// 기존 데이터 메모리해제
		if (data) data = NULL;

		// 데이터 복사
		data = other.data;

		// 이동 데이터 메모리해제
		other.data = NULL;

		cout << this << " 이동대입연산자" << endl;
		return *this;
	}

public:
	TEST() { cout << this << " - 생성자 호출" << endl; }
	~TEST() { cout << this << " - 소멸자 호출" << endl; }
};

int main()
{
	std::vector<TEST> tests;
	tests.reserve(4);
	//cout << "vector size: " << tests.size() << endl;
	//cout << "vector capacity: " << tests.capacity() << endl;

	for (int i = 0; i < 3; ++i)
	{
		cout << endl;
		cout << "================객체 삽입=================" << endl;
		tests.emplace_back(TEST());
		//cout << "vector size: " << tests.size() << endl;
		//cout << "vector capacity: " << tests.capacity() << endl;
	}

	cout << endl;
	cout << "==============중간에 삽입===================" << endl;
	std::vector<TEST>::iterator iter = tests.begin() + 1;
	tests.insert(iter,TEST());

	system("pause");
}