#include <iostream>
using namespace std;

// [질문] 레퍼런스를 왜 사용합니까? 어디에 사용합니까?
// &는 주소를 의미하지 않고 참조를 의미한다.

void change(int& a, int& b)
{
	int temp{ a };
	a = b;
	b = temp;
}

int main()
{
	int a{ 1 };
	int b{ 2 };

	change(a, b);

	cout << a << ", " << b << endl;
}