#include <iostream>
using namespace std;

class X
{
public:
	explicit X(int data) : m_Data(data) {
		cout << "기본 생성자" << endl;
	};
	explicit X(const X& rhs) {
		cout << "복사생성자" << endl;
		m_Data = rhs.m_Data; 
	}
	explicit X(X&& rhs)
	{
		cout << "이동생성자" << endl;
		m_Data = rhs.m_Data;
	}
	virtual ~X() = default;

	void SetData(int data) { m_Data = data; }
	int GetData() const { return m_Data; }

	void operator=(const X& rhs) 
	{
		m_Data = rhs.GetData();
	}
	void operator=(X&& rhs)
	{
		m_Data = rhs.m_Data;

		rhs.m_Data = 0;
	}

	friend ostream& operator<<(ostream& os, const X& a);

private:
	int m_Data;
};

ostream& operator<<(ostream& os, const X& a)
{
	os << a.m_Data;
	return os;
}


void change(X& a, X& b)
{
	X t{ a };
	a = b;
	b = t;
}

int main()
{
	X a{ 1 };
	X b{ 2 };

	change( a,b );

	cout << a << ", " << b << endl;
}